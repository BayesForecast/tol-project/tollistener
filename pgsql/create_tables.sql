-----------------------------------------------------------------------------------------
DROP TABLE IF EXISTS tolstn_f_status;
CREATE TABLE tolstn_f_status
-----------------------------------------------------------------------------------------
(
  co_host character varying(255) NOT NULL,
  id_tol_pid integer NOT NULL,
  co_status character varying(255) NOT NULL,
  co_task character varying(255) NOT NULL,
  te_task text NOT NULL,
  co_return character varying(255) NOT NULL,
  te_return text,
  dt_last_ping timestamp without time zone,
  CONSTRAINT pk_tolstn_f_status PRIMARY KEY (co_host, id_tol_pid)
)
WITH (OIDS=FALSE);
ALTER TABLE tolstn_f_status  OWNER TO toldevel;
CREATE INDEX idx_tolstn_f_status_co_host ON tolstn_f_status (co_host);
CREATE INDEX idx_tolstn_f_status_co_status ON tolstn_f_status (co_status);
CREATE INDEX idx_tolstn_f_status_dt_last_ping ON tolstn_f_status (dt_last_ping);

-----------------------------------------------------------------------------------------
DROP TABLE IF EXISTS tolstn_f_queue;
CREATE TABLE tolstn_f_queue
-----------------------------------------------------------------------------------------
(
  co_task character varying(255) NOT NULL,
  dt_request timestamp without time zone NOT NULL, 
  te_task text NOT NULL,
  co_return character varying(255) NOT NULL,
  te_return text,
  co_status character varying(255) NOT NULL,
  co_host character varying(255),
  id_tol_pid integer,
  dt_last_ping timestamp without time zone,
  CONSTRAINT pk_tolstn_f_queue PRIMARY KEY (co_task, dt_request)
)
WITH (OIDS=FALSE);
ALTER TABLE tolstn_f_queue OWNER TO toldevel;
CREATE INDEX idx_tolstn_f_queue_co_task ON tolstn_f_queue (co_task);
CREATE INDEX idx_tolstn_f_queue_co_status ON tolstn_f_queue (co_status);
CREATE INDEX idx_tolstn_f_queue_co_return ON tolstn_f_queue (co_return);
CREATE INDEX idx_tolstn_f_queue_co_host ON tolstn_f_queue (co_host);
CREATE INDEX idx_tolstn_f_queue_id_tol_pid ON tolstn_f_queue (id_tol_pid);
CREATE INDEX idx_tolstn_f_queue_dt_dt_request ON tolstn_f_queue (dt_request);
CREATE INDEX idx_tolstn_f_queue_dt_last_ping ON tolstn_f_queue (dt_last_ping);


