-----------------------------------------------------------------------------------------
-- FILE: create_functions.sql
-- Ceate all needed functions to work with TolListener as TOL-ODBC querying system
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS wait_tol_process(tol_pid integer, status text);
DROP FUNCTION IF EXISTS get_tol_tab(task text, return_ text, tol_expression text);
DROP FUNCTION IF EXISTS get_tol_pid(task text);
DROP FUNCTION IF EXISTS wait_tol_result(task text);
DROP FUNCTION IF EXISTS set_tol_wait(task text);
DROP FUNCTION IF EXISTS kill_old_tol_pid();
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
CREATE FUNCTION wait_tol_process(tol_pid integer, status text)
RETURNS integer AS $body$
-----------------------------------------------------------------------------------------
DECLARE
  sleep_count real;
BEGIN
  RAISE NOTICE 'TRACE [wait_tol_process] WAITING ...';
  IF tol_pid > 0
  THEN WHILE (SELECT count(*) FROM tolstn_f_status 
              WHERE id_tol_pid=tol_pid 
                AND co_status= status) = 0 
    LOOP 
      EXECUTE (select pg_sleep(1));
      RAISE NOTICE '.';
    END LOOP;
  ELSE WHILE (SELECT count(*) FROM tolstn_f_status 
              WHERE co_status= status) = 0 
    LOOP 
      EXECUTE (select pg_sleep(1));
      RAISE NOTICE '|';
    END LOOP;
  END IF;              
  RAISE NOTICE 'TRACE [wait_tol_process] FOUND! ';
  return (SELECT id_tol_pid FROM tolstn_f_status 
          WHERE co_status= status
          ORDER BY dt_last_ping desc limit 1);
END; 
$body$ LANGUAGE 'plpgsql' VOLATILE CALLED ON NULL INPUT SECURITY INVOKER;

-----------------------------------------------------------------------------------------
CREATE FUNCTION get_tol_tab(task text, return_ text, tol_expression text)
RETURNS integer AS $body$
-----------------------------------------------------------------------------------------
DECLARE
  tol_pid integer;
  tol_pid_2 integer;
  update_result real;
  sleep_count real;
  tol_return text;
BEGIN
  RAISE NOTICE 'TRACE [get_tol_tab] 1 ';
  tol_pid := wait_tol_process(-1,'WAIT');
  tol_return := task;
  IF  (select count(*) from pg_tables where schemaname='public' 
       and tablename = tol_return) = 1 
  THEN
    EXECUTE 'DROP TABLE '||tol_return;
  END IF;
  RAISE NOTICE 'TRACE [get_tol_tab] 2 %', tol_pid;
  UPDATE tolstn_f_status  
    SET co_task = task, te_task = tol_expression, co_return = return_, co_status = 'CALL'
    WHERE id_tol_pid = tol_pid ;
  RAISE NOTICE 'TRACE [get_tol_tab] 3 ';
  return (tol_pid);
END; 
$body$ LANGUAGE 'plpgsql' VOLATILE CALLED ON NULL INPUT SECURITY INVOKER;


-----------------------------------------------------------------------------------------
CREATE FUNCTION get_tol_pid(task text)
RETURNS integer AS $body$
-----------------------------------------------------------------------------------------
BEGIN
  RAISE NOTICE 'TRACE [get_tol_pid] 1 ';
  return (select id_tol_pid from tolstn_f_status where co_task=task limit 1);
END; 
$body$ LANGUAGE 'plpgsql' VOLATILE CALLED ON NULL INPUT SECURITY INVOKER;


-----------------------------------------------------------------------------------------
CREATE FUNCTION wait_tol_result(task text)
RETURNS integer AS $body$
-----------------------------------------------------------------------------------------
DECLARE
  tol_pid integer;
  tol_return text;
BEGIN
  tol_pid := get_tol_pid(task);
  RAISE NOTICE 'TRACE [wait_tol_result] 1 ';
  tol_return := task;
--tol_pid_2 := wait_tol_process(tol_pid,'WAIT');
 WHILE (select count(*) from pg_tables where schemaname='public' 
   and tablename = tol_return) = 0 
    LOOP 
      EXECUTE (select pg_sleep(1));
      RAISE NOTICE '=';
    END LOOP;
  RAISE NOTICE 'TRACE [wait_tol_result] 2 ';
  return (tol_pid);
END; 
$body$ LANGUAGE 'plpgsql' VOLATILE CALLED ON NULL INPUT SECURITY INVOKER;


-----------------------------------------------------------------------------------------
CREATE FUNCTION set_tol_wait(task text)
RETURNS integer AS $body$
-----------------------------------------------------------------------------------------
DECLARE
  tol_pid integer;
BEGIN
  tol_pid := get_tol_pid(task);
  RAISE NOTICE 'TRACE [set_tol_wait] 1 ';
  UPDATE tolstn_f_status  
    SET co_status = 'WAIT'
    WHERE id_tol_pid = tol_pid ;
  return (tol_pid);
END; 
$body$ LANGUAGE 'plpgsql' VOLATILE CALLED ON NULL INPUT SECURITY INVOKER;


-----------------------------------------------------------------------------------------
CREATE FUNCTION kill_old_tol_pid()
RETURNS integer AS $body$
-----------------------------------------------------------------------------------------
BEGIN
  update tolstn_f_status set co_status = 'KILL' where now() - dt_last_ping > interval '1 day';
  delete from tolstn_f_status where co_status = 'KILL' and now() - dt_last_ping > interval '2 day';
  return 1;
END; 
$body$ LANGUAGE 'plpgsql' VOLATILE CALLED ON NULL INPUT SECURITY INVOKER;


--select * from get_tol_tab_ser('tol_return_3140_4');
